import cv2
import numpy as np
import sys
import getopt
import argparse

class Heatmap:
	name = ''
	heatmap = []

	def __init__(self, name, image):
		self.name = name
		self.heatmap = np.zeros_like(image,  dtype = np.uint8)
		self.heatmap = cv2.cvtColor(self.heatmap, cv2.COLOR_RGB2GRAY)

	def update(self, toAdd):
		toAdd = cv2.cvtColor(toAdd, cv2.COLOR_RGB2GRAY)
		self.heatmap = cv2.addWeighted(self.heatmap, 0.3, toAdd, 1, 1);
		return self.heatmap

#TODO make it class
def canny(image):
	gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
	blur = cv2.GaussianBlur(gray, (5,5), 0)
	canny = cv2.Canny(blur, 50, 150)
	return canny

def display_line(image, lines):
	line_image = np.zeros_like(image)
	if lines is not None:
			for line in lines:
				x1, y1, x2, y2 = line.reshape(4)
				cv2.line(line_image, (x1, y1), (x2, y2), (0, 255, 0), 10)
	return line_image

def resizeByPercentage(image, scale_percent):
	width = int(image.shape[1] * scale_percent / 100)
	height = int(image.shape[0] * scale_percent / 100)
	dim = (width, height)
	# resize image
	resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
	return resized

class RoiCalibrator:
	points = [(1,1),(1,1),(1,1),(1,1), (0,164), (0,632), (1343,632), (1343,164)]
	name = ''

	def __init__(self, name):
		self.name = name

	def calibrateRoiCallback(self, event, x, y, flags, param):
		if event == cv2.EVENT_LBUTTONDOWN:
			print(self.name + " :: x=" + str(x) + " y=" + str(y))
			self.points.append([x, y])

	def calibrate(self, image):
		cv2.namedWindow(self.name)
		cv2.setMouseCallback(self.name, self.calibrateRoiCallback)
		cv2.imshow(self.name, image)
		while(True):
			if cv2.waitKey(1) == ord('q'):
				break
		print(self.name + ' was calibrated!')
		cv2.setMouseCallback(self.name, lambda: None)
		cv2.destroyWindow(self.name)

	def roi1(self, image):
		triangle = np.array([
		[(self.points[0]), (self.points[1]), (self.points[2]), (self.points[3])]])	
		mask = np.zeros_like(image)
		filtr = cv2.fillPoly(mask, triangle, 255)
		masked_image = cv2.bitwise_and(image, mask)
		return masked_image, filtr 

	def roi2(self, image):
		triangle = np.array([
		[(self.points[4]), (self.points[5]), (self.points[6]), (self.points[7])]])	
		mask = np.zeros_like(image)
		filtr = cv2.fillPoly(mask, triangle, 255)
		masked_image = cv2.bitwise_and(image, mask)
		return masked_image, filtr 

# class YoloCarDetector:
# 	detector = []
# 	outs = []
# 	blob = []
# 	net = []

# 	def configure(self, path):
# 		# Load names of classes
# 		classesFile = path + "names.txt";
# 		classes = None
# 		with open(classesFile, 'rt') as f:
# 		    classes = f.read().rstrip('\n').split('\n')

# 		# Give the configuration and weight files for the model and load the network using them.
# 		modelConfiguration = path + "yolov3.cfg";
# 		modelWeights = path + "yolov3.weights";

# 		self.net = cv.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
# 		self.net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
# 		self.net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)

# 	def detect(self, image):
# 	    self.blob = cv.dnn.blobFromImage(image, 1/255, (image.width, image.height), [0,0,0], 1, crop=False)
#         self.net.setInput(self. blob)
#     	self.outs = net.forward(getOutputsNames(net))
#         self.postprocess(image, outs)
# 		return cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

# 	def getOutputsNames(net):
#         layersNames = net.getLayerNames()
#     	return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

#     # Remove the bounding boxes with low confidence using non-maxima suppression
# 	def postprocess(frame, outs):
# 	    frameHeight = frame.shape[0]
# 	    frameWidth = frame.shape[1]

# 	    classIds = []
# 	    confidences = []
# 	    boxes = []
# 	    classIds = []
# 	    confidences = []
# 	    boxes = []
# 	    for out in outs:
# 	        for detection in out:
# 	            scores = detection[5:]
# 	            classId = np.argmax(scores)
# 	            confidence = scores[classId]
# 	            if confidence > confThreshold:
# 	                center_x = int(detection[0] * frameWidth)
# 	                center_y = int(detection[1] * frameHeight)
# 	                width = int(detection[2] * frameWidth)
# 	                height = int(detection[3] * frameHeight)
# 	                left = int(center_x - width / 2)
# 	                top = int(center_y - height / 2)
# 	                classIds.append(classId)
# 	                confidences.append(float(confidence))
# 	                boxes.append([left, top, width, height])

# 	    indices = cv.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)
# 	    for i in indices:
# 	        i = i[0]
# 	        box = boxes[i]
# 	        left = box[0]
# 	        top = box[1]
# 	        width = box[2]
# 	        height = box[3]
# 	        drawPred(classIds[i], confidences[i], left, top, left + width, top + height)

class CarDetector:
	detector = []

	def configure(self, path):
		self.detector = cv2.CascadeClassifier(path)


	def detect(self, image):
		if(not self.detector.empty()):
			found = self.detector.detectMultiScale(image, 10, 4, 0, (60,40))
			cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
			image = np.zeros_like(image)
			if(len(found) > 0):
				print("Found:" + str(len(found)))
			for x1, y1, x2, y2 in found:
				cv2.rectangle(image, (x1, y1), (x1+x2, y1+y2), (255, 255, 0), cv2.FILLED)
		return cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

def processLines(processing_image, roiCalibrator):
	canny_image = canny(processing_image)
	cropped_image, triangle = roiCalibrator.roi1(canny_image)
	triangle = cv2.cvtColor(triangle, cv2.COLOR_GRAY2RGB)
	lines = cv2.HoughLinesP(cropped_image, 4, np.pi/180, 100, np.array([]), minLineLength=20, maxLineGap=8)
	line_image = display_line(processing_image, lines)
	roi = cv2.addWeighted(image, 1,  triangle, 0.1, 1)
	return triangle, line_image

def setup(cap):
	while(True):
		_, image = cap.read()
		# image  = resizeByPercentage(image, 70)
		cv2.imshow('calibrate', image)
		if cv2.waitKey(1) == ord('q'):
			break
	cv2.destroyWindow('calibrate')


# main
cap = cv2.VideoCapture('radek1.mp4')
# cap = cv2.VideoCapture(0)
roiCalibrator = RoiCalibrator('ROI for lanes')
roiCalibratorForDetector = RoiCalibrator('ROI for Detector')

# IF haar
car = CarDetector()
car.configure('checkcas.xml')

# IF YOLO
# yoloCar = YoloCarDetector()
# yoloCar.configure('')

while(not cap.isOpened()):
	pass

setup(cap)

_, image = cap.read()
image = cv2.resize(image, None, fx=0.7, fy=0.7)
# image  = resizeByPercentage(imageBig, 70)
roiCalibrator.calibrate(image)
roiCalibratorForDetector.calibrate(image)

heatmapCars  = Heatmap('FoundCars', image)
heatmapLanes = Heatmap('Lanes', image)

height = image.shape[0]
width = image.shape[1]

writerForHeat =  cv2.VideoWriter('Heat.mp4', cv2.VideoWriter_fourcc(*'MP4V'), 25, (width, height))
writer =  cv2.VideoWriter('output.mp4', cv2.VideoWriter_fourcc(*'MP4V'), 25, (width, height))

ffw = False
RUN = False

while(cap.isOpened() and RUN):
	_, image = cap.read()
	image = cv2.resize(image, None, fx=0.7, fy=0.7)
	output = image
	if(not ffw):

		imageForDetector, triangle2 = roiCalibratorForDetector.roi2(cv2.cvtColor(image, cv2.COLOR_RGB2GRAY))
		foundCarsFrames = car.detect(imageForDetector)
		
		lanesRoi, lanes = processLines(image, roiCalibrator)		

		output = cv2.addWeighted(output, 1, lanesRoi, 0.1, 1) # adding roi
		output = cv2.addWeighted(output, 1, cv2.cvtColor(triangle2, cv2.COLOR_GRAY2RGB), 0.1, 1) #adding roi
		output = cv2.addWeighted(output, 1, foundCarsFrames, 1, 1) # adding found cars
		output = cv2.addWeighted(output, 1, lanes, 1, 1) # adding found lanes
		# output = cv2.addWeighted(output, 0.8, image, 1, 1) #adding orginal frame to found objects
		fromMapCars  = heatmapCars.update(foundCarsFrames)
		fromMapLanes = heatmapLanes.update(lanes)
		heat = cv2.cvtColor(cv2.addWeighted(fromMapCars, 1, fromMapLanes, 1, 0), cv2.COLOR_GRAY2RGB);
		heat = cv2.addWeighted(image, 0.8, heat ,1, 1)

		writerForHeat.write(heat)
		writer.write(output)

	cv2.imshow("FromMap", heat)
	cv2.imshow('Processed', output)
	if cv2.waitKey(1) == ord('q'):
		break
	if cv2.waitKey(1) == ord('f'):
		ffw = True
	if cv2.waitKey(1) == ord('g'):
		ffw = False
cap.release()
writer.release()
writerForHeat.release()
cv2.destroyAllWindows()
